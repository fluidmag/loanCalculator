<%-- 
    Document   : display_loan.jsp
    Created on : 19-Apr-2016, 5:29:54 PM
    Author     : Chang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Standard Servlet/JSP Example</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>

    <body>
        <h1>Thanks for using this loan calculator</h1>

        <p>Here is the information that you entered:</p>
        <label>Present Value:</label>
        <span>${user.presentValue}</span><br>
        <label>Interest Rate:</label>
        <span>${user.interestRate}</span><br>
        <label>Number of Terms:</label>
        <span>${user.numTerm}</span><br>
        <label>Loan to Pay in Each Term</span><br>
        <span>${userIO.loanPay}</span><br>

        <p>To make another calculation, click on the Return button shown below.</p>

        <form action="loanCalculatorJSP.jsp" method="post">
            <input type="submit" value="Return">
        </form>
    </body>




</html>