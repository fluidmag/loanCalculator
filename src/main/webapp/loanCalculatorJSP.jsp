<%-- 
    Document   : loanCalculatorJSP
    Created on : Apr 12, 2016, 8:46:08 PM
    Author     : cha_xi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Loan Calculator </title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>
    <body>
        <h1>Input Info</h1>
        <p>To calculate monthly payment, input present value(PV), annual interest rate and number of terms.</p>

        <form action="LoanCalculator" method="get">
            <label class="pad_top">Present Value:</label>
            <input type="text" name="presentValue" required><br>
            <label class="pad_top">Interest rate:</label>
            <input type="text" name="interestRate" required><br>
            <label class="pad_top">Number of terms:</label>
            <input type="text" name="numTerm" required><br>
            <label>&nbsp;</label>
            <input type="submit" value="monthly payment" class="margin_left">
        </form>
    </body>
</html>
