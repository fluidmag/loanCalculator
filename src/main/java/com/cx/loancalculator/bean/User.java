/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cx.loancalculator.bean;

/**
 *
 * @author cha_xi
 */
public class User
{
    private String interestRate;
    private String numTerm;
    private String presentValue;
    
    public User()
    {
        interestRate = "";
        numTerm = "";
        presentValue = "";
    }
    
    public User(String interestRate, String numTerm, String presentValue)
    {
        this.interestRate = interestRate;
        this.numTerm = numTerm;
        this.presentValue = presentValue;
    }
    
    public void setInterestRate(String interestRate)
    {
        this.interestRate = interestRate;
    }

    public String getInterestRate()
    { 
        return interestRate; 
    }
    
    public void setNumTerm(String numTerm)
    {
        this.numTerm = numTerm;
    }

    public String getNumTerm()
    { 
        return numTerm; 
    }
    
    public void setPresentValue(String presentValue)
    {
        this.presentValue = presentValue;
    }

    public String getPresentValue()
    { 
        return presentValue; 
    }
    
    
    //////////////////////

    public double loan()
    {
        return Double.valueOf(presentValue)*Double.valueOf(interestRate)
                /(1-Math.pow(1+Double.valueOf(interestRate),-Double.valueOf(numTerm)));
    }

    ///////////////////////////

    @Override
    public String toString() {
        return "User{" + "interestRate=" + interestRate + ", numTerm=" + numTerm + ", presentValue=" + presentValue + '}';
    }
}
