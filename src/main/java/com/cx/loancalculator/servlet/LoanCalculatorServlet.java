/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cx.loancalculator.servlet;



 import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.cx.loancalculator.bean.User;
import com.cx.loancalculator.business.UserIO;
/**
 *
 * @author cha_xi
 */
@WebServlet(name = "LoanCalculator", urlPatterns = {"/LoanCalculator"})
public class LoanCalculatorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/display_loan.jsp";
        //get parameters from requests;
        String presentValue = request.getParameter("presentValue");
        String interestRate = request.getParameter("interestRate");
        String numTerm = request.getParameter("numTerm");
        
        // store data in user object
        User user = new User(presentValue,interestRate,numTerm);
        
        // get the actual path on this server and add it to where the file must be written.
        HttpSession session = request.getSession();
        ServletContext context = session.getServletContext();
        String realContextPath = context.getRealPath("/WEB-INF/LoanCalculator.txt");
        
        // write the User object to a file
        UserIO userIO = new UserIO();
        userIO.loanPayment(user);
        request.setAttribute("user", user);
        request.setAttribute("userIO", userIO);
        
        // forward request and response to JSP page
        RequestDispatcher dispatcher
                = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
     
        }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        doPost(request, response);
    }
 

}
